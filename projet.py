import tkinter as tk
from tkinter import ttk, filedialog
import pandas as pd
import numpy as np
from scipy.io import arff

def entropy(labels):
    _, counts = np.unique(labels, return_counts=True)
    probs = counts / len(labels)
    return -np.sum(probs * np.log2(probs))

def information_gain(data, split_column_name, target_column_name):
    original_entropy = entropy(data[target_column_name])
    
    weighted_entropy = 0
    total_rows = len(data)
    
    for value in data[split_column_name].unique():
        subset = data[data[split_column_name] == value]
        subset_entropy = entropy(subset[target_column_name])
        weighted_entropy += (len(subset) / total_rows) * subset_entropy
    
    information_gain = original_entropy - weighted_entropy
    return information_gain


def choose_best_column(data, label):
    best_gain = -1
    best_column = None
    
    for column in data.columns:
        if column != label:
            gain = information_gain(data, column, label)
            print(column, gain)
            if gain > best_gain:
                best_gain = gain
                best_column = column
    
    return best_column

def create_single_rule(df, label):
    best_column = choose_best_column(df, label)

    best_value = df[best_column].value_counts().idxmax()
    
    filtered_df = df.loc[df[best_column] == best_value]
    predict_value = filtered_df[label].value_counts().idxmax()

    return dict(col_name=best_column, value=best_value, predict=predict_value)

def build_decision_tree(df, label, max_depth):
    if max_depth == 0:
        return None
    
    if len(df[label].unique()) == 1:
        return df[label].iloc[0]
    
    best_rule = create_single_rule(df, label)
    
    left_data = df[df[best_rule['col_name']] != best_rule['value']]
    right_data = df[df[best_rule['col_name']] == best_rule['value']]
    
    left_branch = build_decision_tree(left_data, label, max_depth - 1)
    right_branch = build_decision_tree(right_data, label, max_depth - 1)
    
    return {'rule': best_rule, 'left': left_branch, 'right': right_branch}

def predict(tree, sample):
    if isinstance(tree, dict):
        col_name = tree['rule']['col_name']
        value = tree['rule']['value']
        if sample[col_name] != value:
            return predict(tree['left'], sample)
        else:
            return predict(tree['right'], sample)
    else:
        return tree

def accuracy(tree, data, label):
    correct_predictions = 0
    total_samples = len(data)
    
    for _, sample in data.iterrows():
        prediction = predict(tree, sample)
        if prediction == sample[label]:
            correct_predictions += 1
    
    return correct_predictions / total_samples

# Fonction pour élaguer l'arbre en fonction de la précision de validation
def prune_tree(tree, validation_data, label):
    if not isinstance(tree, dict):
        return tree
    
    left_branch = tree['left']
    right_branch = tree['right']
    
    if not isinstance(left_branch, dict) and not isinstance(right_branch, dict):
        original_accuracy = accuracy(tree, validation_data, label)
        
        majority_class = validation_data[label].value_counts().idxmax()
        validation_data_size = len(validation_data)
        majority_class_accuracy = len(validation_data[validation_data[label] == majority_class]) / validation_data_size
        if majority_class_accuracy >= original_accuracy:
            return majority_class
    
    tree['left'] = prune_tree(left_branch, validation_data, label)
    tree['right'] = prune_tree(right_branch, validation_data, label)
    
    return tree

# Main GUI
class DecisionTreeGUI(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("Decision Tree Builder")
        self.geometry("400x200")
        self.pages = {}

        # Create main page
        main_page = MainPage(self)
        self.pages["main"] = main_page
        main_page.grid(row=0, column=0, sticky="nsew")

        # Create train page
        train_page = TrainPage(self)
        self.pages["train"] = train_page
        train_page.grid(row=0, column=0, sticky="nsew")

        # Create test page
        test_page = TestPage(self)
        self.pages["test"] = test_page
        test_page.grid(row=0, column=0, sticky="nsew")

        # Show main page initially
        self.show_page("main")

    def show_page(self, page_name):
        page = self.pages[page_name]
        page.tkraise()

# Main Page
class MainPage(ttk.Frame):
    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent
        label = ttk.Label(self, text="Welcome to Decision Tree Builder")
        label.pack(pady=20)

        # Button to switch to training page
        train_button = ttk.Button(self, text="Train Model", command=lambda: parent.show_page("train"))
        train_button.pack()

        # Button to switch to testing page
        test_button = ttk.Button(self, text="Test Model", command=lambda: parent.show_page("test"))
        test_button.pack()

# Train Page
class TrainPage(ttk.Frame):
    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent
        self.train_file_path = ""
        self.train_button = ttk.Button(self, text="Back", command=lambda: parent.show_page("main"))
        self.train_button.pack()
        label = ttk.Label(self, text="Train Your Decision Tree Model")
        label.pack(pady=20)

        # Button to choose training data file
        self.train_file_button = ttk.Button(self, text="Choose Training Data File", command=self.choose_train_file)
        self.train_file_button.pack()

        # Entry for max depth
        label = ttk.Label(self, text="max depth")
        label.pack(pady=10)
        self.max_depth_entry = ttk.Entry(self, width=10)
        self.max_depth_entry.pack()
        
        # Entry for label
        label = ttk.Label(self, text="Label")
        label.pack(pady=10)
        self.label_entry = ttk.Entry(self, width=10)
        self.label_entry.pack()

        # Button to start training
        self.train_button = ttk.Button(self, text="Start Training", command=self.train_model)
        self.train_button.pack()

        # Label to show result
        self.result_label = ttk.Label(self, text="")
        self.result_label.pack()

        # Button to download result
        self.download_button = ttk.Button(self, text="Download Result", command=self.download_result, state=tk.DISABLED)
        self.download_button.pack()

    def choose_train_file(self):
        self.train_file_path = filedialog.askopenfilename(filetypes=[("ARFF Files", "*.arff")])
        print("Training file:", self.train_file_path)  # Replace with actual processing of the file
        if self.train_file_path:
            self.train_file_button.config(text="Training File Selected")

    def train_model(self):
        if not self.train_file_path:
            print("Please choose a training data file.")
            return
        max_depth = int(self.max_depth_entry.get())
        label = self.label_entry.get()
        arff_file = arff.loadarff(self.train_file_path)
        data = pd.DataFrame(arff_file[0])
        tree = prune_tree(build_decision_tree(data, label, max_depth), data, label)
        accuracy_val = accuracy(tree, data, label)
        accuracy_percentage = round(accuracy_val * 100, 2)
        self.tree = tree
        self.result_label.config(text=f"Decision Tree built with accuracy: {accuracy_percentage}%")
        self.download_button.config(state=tk.NORMAL)

    def download_result(self):
        # Replace this with actual code to generate and download the result file
        print(self.tree)

# Test Page
class TestPage(ttk.Frame):
    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent
        self.test_file_path = ""
        self.model_file_path = ""
        label = ttk.Label(self, text="Test Your Decision Tree Model")
        label.pack(pady=20)

        # Button to choose test data file
        self.test_file_button = ttk.Button(self, text="Choose Test Data File", command=self.choose_test_file)
        self.test_file_button.pack()

        # Button to choose model file
        self.model_file_button = ttk.Button(self, text="Choose Model File", command=self.choose_model_file)
        self.model_file_button.pack()

        # Button to start testing
        self.test_button = ttk.Button(self, text="Start Testing", command=self.test_model)
        self.test_button.pack()

    def choose_test_file(self):
        self.test_file_path = filedialog.askopenfilename(filetypes=[("ARFF Files", "*.arff")])
        print("Test file:", self.test_file_path)  # Replace with actual processing of the file
        if self.test_file_path:
            self.test_file_button.config(text="Test Data File Selected")

    def choose_model_file(self):
        self.model_file_path = filedialog.askopenfilename(filetypes=[("Model Files", "*.txt")])
        print("Model file:", self.model_file_path)  # Replace with actual loading of the model
        if self.model_file_path:
            self.model_file_button.config(text="Model File Selected")

    def test_model(self):
        if not self.test_file_path or not self.model_file_path:
            print("Please choose both a test data file and a model file.")
            return
        print("Testing model...")  # Replace with actual testing logic

if __name__ == "__main__":
    app = DecisionTreeGUI()
    app.mainloop()
